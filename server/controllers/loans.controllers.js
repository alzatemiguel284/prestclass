import db from "../db.js";
import { getLoans } from "../models/loans.js";

export const postLoan = async (req, res) => {
  try {
    const {
      body: { loans },
      user,
    } = req;

    for await (const loan of loans) {
      await db.query("CALL loans_save($1, $2, $3, $4, $5)", [
        loan.loa_hour_initial,
        loan.loa_hour_final,
        loan.loa_date,
        user.id,
        loan.loa_roo_id,
      ]);
    }

    return res.json({ message: "Loan created successfully" });
  } catch (error) {
    console.error(error);
  }
};

export const getAllLoans = async (req, res) => {
  try {
    const { usr_id, roo_id, sta_id } = req.query;

    const loans = await getLoans(null, usr_id, roo_id, sta_id);

    return res.json(loans);
  } catch (error) {
    console.error(error);
  }
};

export const getLoanById = async (req, res) => {
  try {
    const { loan } = req.body;

    return res.json(loan);
  } catch (error) {
    console.error(error);
  }
};

export const putLoan = async (req, res) => {
  try {
    const {
      body: {
        loa_hour_initial,
        loa_hour_final,
        loa_date,
        loa_roo_id,
        loa_sta_id,
        loan,
      },
      user,
    } = req;

    await db.query("CALL loans_update($1, $2, $3, $4, $5, $6, $7)", [
      loan.loa_id,
      loa_hour_initial ? loa_hour_initial : loan.loa_hour_initial,
      loa_hour_final ? loa_hour_final : loan.loa_hour_final,
      loa_date ? loa_date : loan.loa_date,
      user.id,
      loa_roo_id ? loa_roo_id : loan.loa_roo_id,
      loa_sta_id ? loa_sta_id : loan.loa_sta_id,
    ]);

    return res.json({ message: "Loan updated successfully" });
  } catch (error) {
    console.error(error);
  }
};

export const deleteLoan = async (req, res) => {
  try {
    const { id } = req.params;

    await db.query("CALL loans_disable($1)", [id]);

    return res.json({ message: "Loan disabled successfully" });
  } catch (error) {
    console.error(error);
  }
};
