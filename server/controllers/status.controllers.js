import { getStatus } from "../models/status.js";

export const getAllStatus = async (req, res) => {
  try {
    const status = await getStatus();

    return res.json(status);
  } catch (error) {
    console.error(error);
  }
};
