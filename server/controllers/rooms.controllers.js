import db from "../db.js";
import { getRooms } from "../models/rooms.js";

export const postRoom = async (req, res) => {
  try {
    const { roo_name, roo_special } = req.body;

    await db.query("CALL rooms_save($1, $2)", [roo_name, roo_special]);

    return res.json({ message: "Room created successfully" });
  } catch (error) {
    console.error(error);
  }
};

export const getAllRooms = async (req, res) => {
  try {
    const { special, sta_id } = req.query;

    const rooms = await getRooms(null, special, sta_id);

    return res.json(rooms);
  } catch (error) {
    console.error(error);
  }
};

export const getRoomById = async (req, res) => {
  try {
    const { room } = req.body;

    return res.json(room);
  } catch (error) {
    console.error(error);
  }
};

export const putRoom = async (req, res) => {
  try {
    const { roo_name, roo_special, roo_sta_id, room } = req.body;

    await db.query("CALL rooms_update($1, $2, $3, $4)", [
      room.roo_id,
      roo_name ? roo_name : room.roo_name,
      roo_special ? roo_special : room.roo_special,
      roo_sta_id ? roo_sta_id : room.roo_sta_id,
    ]);

    return res.json({ message: "Room updated successfully" });
  } catch (error) {
    console.error(error);
  }
};

export const deleteRoom = async (req, res) => {
  try {
    const { id } = req.params;

    await db.query("CALL rooms_disable($1)", [id]);

    return res.json({ message: "Room disabled successfully" });
  } catch (error) {
    console.error(error);
  }
};
