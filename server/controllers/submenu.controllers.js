import db from "../db.js";
import { getSubmenus } from "../models/submenu.js";

export const getAllSubmenus = async (req, res) => {
  try {
    const { rol_id, men_id } = req.query;

    const submenus = await getSubmenus(null, rol_id, men_id);

    return res.json(submenus);
  } catch (error) {
    console.error(error);
  }
};

export const getSubmenuById = async (req, res) => {
  try {
    const { submenu } = req.body;

    return res.json(submenu);
  } catch (error) {
    console.error(error);
  }
};

export const putSubmenu = async (req, res) => {
  try {
    const {
      sub_per_all,
      sub_per_create,
      sub_per_see,
      sub_per_update,
      sub_per_delete,
      sub_is_visible,
      sub_men_id,
      sub_rol_id,
      submenu: sub,
    } = req.body;

    await db.query("CALL submenu_update($1, $2, $3, $4, $5, $6, $7, $8, $9)", [
      sub.sub_id,
      sub_per_all ? sub_per_all : sub.sub_per_all,
      sub_per_create ? sub_per_create : sub.sub_per_create,
      sub_per_see ? sub_per_see : sub.sub_per_see,
      sub_per_update ? sub_per_update : sub.sub_per_update,
      sub_per_delete ? sub_per_delete : sub.sub_per_delete,
      sub_is_visible ? sub_is_visible : sub.sub_is_visible,
      sub_men_id ? sub_men_id : sub.sub_men_id,
      sub_rol_id ? sub_rol_id : sub.sub_rol_id,
    ]);

    return res.json({ message: "Submenu updated successfully" });
  } catch (error) {
    console.error(error);
  }
};
