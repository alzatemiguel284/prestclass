import { getRoles } from "../models/roles.js";

export const getAllRoles = async (req, res, next) => {
  try {
    const roles = await getRoles();

    return res.json(roles);
  } catch (error) {
    console.error(error);
  }
};
