import db from "../db.js";
import { getResources } from "../models/resources.js";

export const postResources = async (req, res) => {
  try {
    const { res_name, res_description, res_cuantity } = req.body;

    await db.query("CALL resources_save($1, $2, $3)", [
      res_name,
      res_description,
      res_cuantity,
    ]);

    return res.json({ message: "Resource created successfully" });
  } catch (error) {
    console.error(error);
  }
};

export const getAllResources = async (req, res) => {
  try {
    const { sta_id } = req.query;

    const resources = await getResources(null, sta_id);

    return res.json(resources);
  } catch (error) {
    console.error(error);
  }
};

export const getResourceById = async (req, res) => {
  try {
    const { resource } = req.body;

    return res.json(resource);
  } catch (error) {
    console.error(error);
  }
};

export const putResource = async (req, res) => {
  try {
    const { res_name, res_description, res_cuantity, res_sta_id, resource } =
      req.body;

    await db.query("CALL resources_update($1, $2, $3, $4, $5)", [
      resource.res_id,
      res_name ? res_name : resource.res_name,
      res_description ? res_description : resource.res_description,
      res_cuantity ? res_cuantity : resource.res_cuantity,
      res_sta_id ? res_sta_id : resource.res_sta_id,
    ]);

    return res.json({ message: "Resource updated successfully" });
  } catch (error) {
    console.error(error);
  }
};

export const deleteResource = async (req, res) => {
  try {
    const { id } = req.params;

    await db.query("CALL resources_disable($1)", [id]);

    return res.json({ message: "Resource disabled successfully" });
  } catch (error) {
    console.error(error);
  }
};
