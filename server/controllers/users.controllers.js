import db from "../db.js";
import { getUsers } from "../models/users.js";

export const getAllUsers = async (req, res) => {
  try {
    const { rol_id, sta_id } = req.query;

    const users = await getUsers(null, null, rol_id, sta_id);

    users.map((e) => delete e.usr_password);

    return res.json(users);
  } catch (error) {
    console.error(error);
  }
};

export const getUserById = async (req, res) => {
  try {
    const { user } = req.body;

    delete user.usr_password;

    return res.json(user);
  } catch (error) {
    console.error(error);
  }
};

export const putUser = async (req, res) => {
  try {
    const { usr_rol_id, usr_sta_id, user } = req.body;

    await db.query("CALL users_update($1, $2, $3, $4, $5, $6, $7)", [
      user.usr_id,
      user.usr_name,
      user.usr_last_name,
      user.usr_email,
      user.usr_password,
      usr_rol_id ? usr_rol_id : user.usr_rol_id,
      usr_sta_id ? usr_sta_id : user.usr_sta_id,
    ]);

    return res.json({ message: "User updated successfully" });
  } catch (error) {
    console.error(error);
  }
};

export const deleteUser = async (req, res) => {
  try {
    const { id } = req.params;

    await db.query("CALL users_disable($1)", [id]);

    return res.json({ message: "User disabled successfully" });
  } catch (error) {
    console.error(error);
  }
};
