import db from "../db.js";
import { getLoaRes } from "../models/loansResources.js";

export const postLoaRes = async (req, res) => {
  try {
    const {
      params: { loa_id },
      body: { resources },
    } = req;

    for await (const res of resources) {
      await db.query("CALL loans_resources_save($1, $2, $3)", [
        res.res_cuantity,
        loa_id,
        res.res_id,
      ]);
    }

    return res.json({ message: "Loans Resources created successfully" });
  } catch (error) {
    console.error(error);
  }
};

export const getAllLoaRes = async (req, res) => {
  try {
    const { loa_id, res_id, sta_id } = req.query;

    const loaRes = await getLoaRes(null, loa_id, res_id, sta_id);

    return res.json(loaRes);
  } catch (error) {
    console.error(error);
  }
};

export const getLoaResById = async (req, res) => {
  try {
    const { loaRes } = req.body;

    return res.json(loaRes);
  } catch (error) {
    console.error(error);
  }
};

export const putLoaRes = async (req, res) => {
  try {
    const { lre_cuantity, lre_loa_id, lre_res_id, lre_sta_id, loaRes } =
      req.body;

    await db.query("CALL loans_resources_update($1, $2, $3, $4, $5)", [
      loaRes.lre_id,
      lre_cuantity ? lre_cuantity : loaRes.lre_cuantity,
      lre_loa_id ? lre_loa_id : loaRes.lre_loa_id,
      lre_res_id ? lre_res_id : loaRes.lre_res_id,
      lre_sta_id ? lre_sta_id : loaRes.lre_sta_id,
    ]);

    return res.json({ message: "Loan Resource updated successfully" });
  } catch (error) {
    console.error(error);
  }
};

export const deleteLoaRes = async (req, res) => {
  try {
    const { id } = req.params;

    await db.query("CALL loans_resources_disable($1)", [id]);

    return res.json({ message: "Loan Resource disabled successfully" });
  } catch (error) {
    console.error(error);
  }
};
