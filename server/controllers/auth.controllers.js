import db from "../db.js";
import { generateToken } from "../libs/jwt.js";

export const register = async (req, res) => {
  try {
    const { usr_name, usr_last_name, usr_email, usr_password } = req.body;

    const {
      rows: [{ inserted_id: usr_id }],
    } = await db.query("CALL users_save($1, $2, $3, $4, 3, null)", [
      usr_name,
      usr_last_name,
      usr_email,
      usr_password,
    ]);

    const token = await generateToken({
      id: usr_id,
      name: usr_name,
      last_name: usr_last_name,
      email: usr_email,
    });

    res.cookie("token", token);

    return res.json({ message: "User registered successfully", usr_id });
  } catch (error) {
    console.error(error);
  }
};

export const login = async (req, res) => {
  try {
    const { user } = req.body;

    const token = await generateToken({
      id: user.usr_id,
      name: user.usr_name,
      last_name: user.usr_last_name,
      email: user.usr_email,
    });

    res.cookie("token", token);

    delete user.usr_password;

    return res.json(user);
  } catch (error) {
    console.error(error);
  }
};

export const logout = (req, res) => {
  res.clearCookie("token");

  return res.json({ message: "User logged out successfully" });
};

export const profile = (req, res) => {
  try {
    const { user } = req.body;

    delete user.usr_password;

    return res.json(user);
  } catch (error) {
    console.error(error);
  }
};

export const putProfile = async (req, res) => {
  try {
    const { usr_name, usr_last_name, usr_email, usr_password, user } = req.body;

    await db.query("CALL users_update($1, $2, $3, $4, $5, $6, $7)", [
      user.usr_id,
      usr_name ? usr_name : user.usr_name,
      usr_last_name ? usr_last_name : user.usr_last_name,
      usr_email ? usr_email : user.usr_email,
      usr_password ? usr_password : user.usr_usr_password,
      user.usr_rol_id,
      user.usr_sta_id,
    ]);

    return res.json({ message: "User updated successfully" });
  } catch (error) {
    console.error(error);
  }
};
