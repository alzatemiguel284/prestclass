import pkg from "pg";
import { DBHOST, DBUSER, DBPASS, DBNAME, DBPORT } from "./config.js";

const { Client } = pkg;

const db = new Client({
  host: DBHOST,
  user: DBUSER,
  password: DBPASS,
  database: DBNAME,
  port: DBPORT,
});

(async () => {
  try {
    await db.connect();
    console.log("DB is connected");
  } catch (error) {
    console.error(error);
  }
})();

export default db;
