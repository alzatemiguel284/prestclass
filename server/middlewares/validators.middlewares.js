import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { SECRET_JWT } from "../config.js";
import { validationResult } from "express-validator";
import { getUsers } from "../models/users.js";
import { getSubmenus } from "../models/submenu.js";
import { getRooms } from "../models/rooms.js";
import { getResources } from "../models/resources.js";
import { getLoans } from "../models/loans.js";
import { getLoaRes } from "../models/loansResources.js";

export const fieldValidator = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) return res.status(400).json(errors);
  return next();
};

export const validateExistingData = async (req, res, next) => {
  try {
    const { usr_email } = req.body;

    const [user] = await getUsers(null, usr_email);

    if (user)
      return res.status(400).json({
        message: "Bad Request",
        description: `The email '${usr_email}' was already resgitserd`,
      });

    next();
  } catch (error) {
    console.error(error);
  }
};

export const encryptPasswordByUserRequest = async (req, res, next) => {
  try {
    const { usr_password } = req.body;

    if (!usr_password) return next();

    req.body.usr_password = await bcrypt.hash(usr_password, 10);

    next();
  } catch (error) {
    console.error(error);
  }
};

export const getUserByEmailAndPassword = async (req, res, next) => {
  try {
    const { usr_email, usr_password } = req.body;

    const [user] = await getUsers(null, usr_email);

    const isMatch = await bcrypt.compare(
      usr_password,
      user.usr_password ? user.usr_password : "."
    );

    if (!isMatch || !user)
      return res.status(400).json({
        message: "Email or password is wrong. Please check it and try again",
      });
    else if (user.usr_sta_id == 2)
      return res
        .status(400)
        .json({ message: "The user is currently inactive" });

    req.body.user = user;
    next();
  } catch (error) {
    console.error(error);
  }
};

export const validateToken = async (req, res, next) => {
  try {
    const { token } = req.cookies;

    if (!token)
      return res.status(401).json({
        message: "Unauthorized",
        description: "You have not logged yet",
      });

    jwt.verify(token, SECRET_JWT, (err, user) => {
      if (err) return err;
      req.user = user;
      next();
    });
  } catch (error) {
    console.error(error);
  }
};

export const getUserProfile = async (req, res, next) => {
  try {
    const { id } = req.user;

    const [user] = await getUsers(id);

    if (!user)
      return res.status(404).json({
        message: "Not Found",
        description: `The id value: ${id} does not exist. Please check it and try again`,
      });

    req.body.user = user;
    next();
  } catch (error) {
    console.error(error);
  }
};

export const getSubmenuId = async (req, res, next) => {
  try {
    const { id } = req.params;

    const [submenu] = await getSubmenus(id);

    if (!submenu)
      return res.status(404).json({
        message: "Not Found",
        description: `The id value: ${id} does not exist. Please check it and try again`,
      });

    req.body.submenu = submenu;
    next();
  } catch (error) {
    console.error(error);
  }
};

export const getUserId = async (req, res, next) => {
  try {
    const { id } = req.params;

    const [user] = await getUsers(id);

    if (!user)
      return res.status(404).json({
        message: "Not Found",
        description: `The id value: ${id} does not exist. Please check it and try again`,
      });

    req.body.user = user;
    next();
  } catch (error) {
    console.error(error);
  }
};

export const getRoomId = async (req, res, next) => {
  try {
    const { id } = req.params;

    const [room] = await getRooms(id);

    if (!room)
      return res.status(404).json({
        message: "Not Found",
        description: `The id value: ${id} does not exist. Please check it and try again`,
      });

    req.body.room = room;
    next();
  } catch (error) {
    console.error(error);
  }
};

export const getResourceId = async (req, res, next) => {
  try {
    const { id } = req.params;

    const [resource] = await getResources(id);

    if (!resource)
      return res.status(404).json({
        message: "Not Found",
        description: `The id value: ${id} does not exist. Please check it and try again`,
      });

    req.body.resource = resource;
    next();
  } catch (error) {
    console.error(error);
  }
};

export const getLoanId = async (req, res, next) => {
  try {
    const { id } = req.params;

    const [loan] = await getLoans(id);

    if (!loan)
      return res.status(404).json({
        message: "Not Found",
        description: `The id value: ${id} does not exist. Please check it and try again`,
      });

    req.body.loan = loan;
    next();
  } catch (error) {
    console.error(error);
  }
};

export const getLoaResId = async (req, res, next) => {
  try {
    const { id } = req.params;

    const [loaRes] = await getLoaRes(id);

    if (!loaRes)
      return res.status(404).json({
        message: "Not Found",
        description: `The id value: ${id} does not exist.Please check it and try again`,
      });

    req.body.loaRes = loaRes;
    next();
  } catch (error) {
    console.error(error);
  }
};
