import express from "express";
import cookieParser from "cookie-parser";
import morgan from "morgan";
import { validateToken } from "./middlewares/validators.middlewares.js";
import authRoutes from "./routes/auth.routes.js";
import rolesRoutes from "./routes/roles.routes.js";
import submenusRoutes from "./routes/submenu.routes.js";
import statusRoutes from "./routes/status.routes.js";
import userRoutes from "./routes/users.routes.js";
import roomsRoutes from "./routes/rooms.routes.js";
import resourcesRoutes from "./routes/resources.routes.js";
import loansRoutes from "./routes/loans.routes.js";
import loaResRoutes from "./routes/loansResources.routes.js";

const app = express();

app.use(express.json());
app.use(cookieParser());
app.use(morgan("dev"));

app.use("/api", validateToken);

app.use("/", authRoutes);
app.use("/api", rolesRoutes);
app.use("/api", submenusRoutes);
app.use("/api", statusRoutes);
app.use("/api", userRoutes);
app.use("/api", roomsRoutes);
app.use("/api", resourcesRoutes);
app.use("/api", loansRoutes);
app.use("/api", loaResRoutes);

export default app;
