import { Router } from "express";
import { body, param, query } from "express-validator";
import {
  fieldValidator,
  getRoomId,
} from "../middlewares/validators.middlewares.js";
import {
  deleteRoom,
  getAllRooms,
  getRoomById,
  postRoom,
  putRoom,
} from "../controllers/rooms.controllers.js";

const router = Router();

router.post(
  "/rooms",
  [
    body("roo_name")
      .isString()
      .notEmpty()
      .withMessage("roo_name is a required field"),
    body("roo_special")
      .isBoolean()
      .notEmpty()
      .withMessage("roo_special is a required field"),
    fieldValidator,
  ],
  postRoom
);
router.get(
  "/rooms",
  [
    query("special")
      .isBoolean()
      .optional()
      .withMessage("special must be a string with a boolean"),
    query("sta_id").isInt().optional().withMessage("sta_id must be an integer"),
    fieldValidator,
  ],
  getAllRooms
);
router.get(
  "/rooms/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    fieldValidator,
    getRoomId,
  ],
  getRoomById
);
router.put(
  "/rooms/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    body("roo_name")
      .isString()
      .optional()
      .withMessage("roo_name must be a string"),
    body("roo_special")
      .isBoolean()
      .optional()
      .withMessage("roo_special must be a string with a boolean"),
    body("roo_sta_id")
      .isInt()
      .notEmpty()
      .withMessage("roo_sta_id must be an integer"),
    fieldValidator,
    getRoomId,
  ],
  putRoom
);
router.delete(
  "/rooms/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    fieldValidator,
    getRoomId,
  ],
  deleteRoom
);

export default router;
