import { Router } from "express";
import { body, param, query } from "express-validator";
import {
  fieldValidator,
  getSubmenuId,
} from "../middlewares/validators.middlewares.js";
import {
  getAllSubmenus,
  getSubmenuById,
  putSubmenu,
} from "../controllers/submenu.controllers.js";

const router = Router();

router.get(
  "/submenu",
  [
    query("rol_id").isInt().optional().withMessage("rol_id must be an integer"),
    query("men_id").isInt().optional().withMessage("men_id must be an integer"),
    fieldValidator,
  ],
  getAllSubmenus
);
router.get(
  "/submenu/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    fieldValidator,
    getSubmenuId,
  ],
  getSubmenuById
);
router.put(
  "/submenu/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    body("sub_per_all")
      .isBoolean()
      .optional()
      .withMessage("sub_per_all must be a boolean"),
    body("sub_per_create")
      .isBoolean()
      .optional()
      .withMessage("sub_per_create must be a boolean"),
    body("sub_per_see")
      .isBoolean()
      .optional()
      .withMessage("sub_per_see must be a boolean"),
    body("sub_per_update")
      .isBoolean()
      .optional()
      .withMessage("sub_per_update must be a boolean"),
    body("sub_per_delete")
      .isBoolean()
      .optional()
      .withMessage("sub_per_delete must be a boolean"),
    body("sub_is_visible")
      .isBoolean()
      .optional()
      .withMessage("sub_is_visible must be a boolean"),
    body("sub_men_id")
      .isInt()
      .optional()
      .withMessage("sub_men_id must be an integer"),
    body("sub_rol_id")
      .isInt()
      .optional()
      .withMessage("sub_rol_id must be an integer"),
    fieldValidator,
    getSubmenuId,
  ],
  putSubmenu
);

export default router;
