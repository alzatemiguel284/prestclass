import { Router } from "express";
import { getAllStatus } from "../controllers/status.controllers.js";

const router = Router();

router.get("/status", getAllStatus);

export default router;
