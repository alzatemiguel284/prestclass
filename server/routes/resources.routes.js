import { Router } from "express";
import { body, param, query } from "express-validator";
import {
  fieldValidator,
  getResourceId,
} from "../middlewares/validators.middlewares.js";
import {
  deleteResource,
  getAllResources,
  getResourceById,
  postResources,
  putResource,
} from "../controllers/resources.controllers.js";

const router = Router();

router.post(
  "/resources",
  [
    body("res_name")
      .isString()
      .notEmpty()
      .withMessage("res_name is a required field"),
    body("res_description")
      .isString()
      .notEmpty()
      .withMessage("res_description is a required field"),
    body("res_cuantity")
      .isInt()
      .notEmpty()
      .withMessage("res_cuantity is a required field"),
    fieldValidator,
  ],
  postResources
);
router.get(
  "/resources",
  [
    query("sta_id").isInt().optional().withMessage("sta_id must be an integer"),
    fieldValidator,
  ],
  getAllResources
);
router.get(
  "/resources/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    fieldValidator,
    getResourceId,
  ],
  getResourceById
);
router.put(
  "/resources/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    body("res_name")
      .isString()
      .optional()
      .withMessage("res_name must be a string"),
    body("res_description")
      .isString()
      .optional()
      .withMessage("res_description must be a string"),
    body("res_cuantity")
      .isInt()
      .optional()
      .withMessage("res_cuantity must be an integer"),
    body("res_sta_id")
      .isInt()
      .optional()
      .withMessage("res_sta_id must be an integer"),
    fieldValidator,
    getResourceId,
  ],
  putResource
);
router.delete(
  "/resources/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    fieldValidator,
    getResourceId,
  ],
  deleteResource
);

export default router;
