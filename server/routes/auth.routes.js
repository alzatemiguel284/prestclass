import { Router } from "express";
import { body, param } from "express-validator";
import {
  encryptPasswordByUserRequest,
  fieldValidator,
  getUserByEmailAndPassword,
  getUserProfile,
  validateExistingData,
} from "../middlewares/validators.middlewares.js";
import {
  login,
  logout,
  profile,
  putProfile,
  register,
} from "../controllers/auth.controllers.js";

const router = Router();

router.post(
  "/public/api/register",
  [
    body("usr_name")
      .isString()
      .notEmpty()
      .withMessage("usr_name is a required field"),
    body("usr_last_name")
      .isString()
      .notEmpty()
      .withMessage("usr_last_name is a required field"),
    body("usr_email")
      .isString()
      .isEmail()
      .notEmpty()
      .withMessage("usr_email is a required field"),
    body("usr_password")
      .isString()
      .isLength({ min: 6 })
      .notEmpty()
      .withMessage("usr_password is a required field"),
    fieldValidator,
    validateExistingData,
    encryptPasswordByUserRequest,
  ],
  register
);
router.post(
  "/public/api/login",
  [
    body("usr_email")
      .isString()
      .isEmail()
      .notEmpty()
      .withMessage("usr_email is a required field"),
    body("usr_password")
      .isString()
      .isLength({ min: 6 })
      .notEmpty()
      .withMessage("usr_password is a required field"),
    fieldValidator,
    getUserByEmailAndPassword,
  ],
  login
);
router.post("/api/logout", logout);
router.get("/api/profile", [getUserProfile], profile);
router.put(
  "/api/profile",
  [
    body("usr_name")
      .isString()
      .optional()
      .withMessage("usr_name must be a string"),
    body("usr_last_name")
      .isString()
      .optional()
      .withMessage("usr_last_name must be a string"),
    body("usr_email")
      .isString()
      .isEmail()
      .optional()
      .withMessage("usr_email must be a string"),
    body("usr_password")
      .isString()
      .isLength({ min: 6 })
      .optional()
      .withMessage("usr_password must be a string"),
    fieldValidator,
    encryptPasswordByUserRequest,
    getUserProfile,
  ],
  putProfile
);

export default router;
