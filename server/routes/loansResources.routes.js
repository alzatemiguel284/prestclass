import { Router } from "express";
import { body, param, query } from "express-validator";
import {
  fieldValidator,
  getLoaResId,
} from "../middlewares/validators.middlewares.js";
import {
  deleteLoaRes,
  getAllLoaRes,
  getLoaResById,
  postLoaRes,
  putLoaRes,
} from "../controllers/loansResources.controllers.js";
import { getLoaRes } from "../models/loansResources.js";

const router = Router();

router.post(
  "/loaRes/:loa_id",
  [
    param("loa_id")
      .isInt()
      .notEmpty()
      .withMessage("The loa_id param is required"),
    body("resources")
      .isArray()
      .notEmpty()
      .withMessage("resources is a required field"),
    fieldValidator,
  ],
  postLoaRes
);
router.get(
  "/loaRes",
  [
    query("loa_id")
      .isInt()
      .optional()
      .withMessage("loa_id is a required field"),
    query("res_id")
      .isInt()
      .optional()
      .withMessage("res_id is a required field"),
    query("sta_id")
      .isInt()
      .optional()
      .withMessage("sta_id is a required field"),
    fieldValidator,
  ],
  getAllLoaRes
);
router.get(
  "/loaRes/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    fieldValidator,
    getLoaResId,
  ],
  getLoaResById
);
router.put(
  "/loaRes/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    fieldValidator,
    getLoaResId,
  ],
  putLoaRes
);
router.delete(
  "/loaRes/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    fieldValidator,
    getLoaResId,
  ],
  deleteLoaRes
);

export default router;
