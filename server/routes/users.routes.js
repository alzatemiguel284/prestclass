import { Router } from "express";
import { body, query, param } from "express-validator";
import {
  fieldValidator,
  getUserId,
} from "../middlewares/validators.middlewares.js";
import {
  deleteUser,
  getAllUsers,
  getUserById,
  putUser,
} from "../controllers/users.controllers.js";

const router = Router();

router.get(
  "/users",
  [
    query("rol_id").isInt().optional().withMessage("rol_id must be an integer"),
    query("sta_id").isInt().optional().withMessage("sta_id must be an integer"),
    fieldValidator,
  ],
  getAllUsers
);
router.get(
  "/users/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    fieldValidator,
    getUserId,
  ],
  getUserById
);
router.put(
  "/users/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    body("usr_rol_id")
      .isInt()
      .optional()
      .withMessage("usr_rol_id msut be an integer"),
    body("usr_sta_id")
      .isInt()
      .optional()
      .withMessage("usr_sta_id msut be an integer"),
    fieldValidator,
    getUserId,
  ],
  putUser
);
router.delete(
  "/users/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    fieldValidator,
    getUserId,
  ],
  deleteUser
);

export default router;
