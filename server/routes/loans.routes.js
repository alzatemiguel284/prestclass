import { Router } from "express";
import { body, param, query } from "express-validator";
import {
  fieldValidator,
  getLoanId,
} from "../middlewares/validators.middlewares.js";
import {
  deleteLoan,
  getAllLoans,
  getLoanById,
  postLoan,
  putLoan,
} from "../controllers/loans.controllers.js";

const router = Router();

router.post(
  "/loans",
  [
    body("loans").isArray().notEmpty().withMessage("loans is a required field"),
    fieldValidator,
  ],
  postLoan
);
router.get(
  "/loans",
  [
    query("usr_id").isInt().optional().withMessage("usr_id must be an integer"),
    query("roo_id").isInt().optional().withMessage("roo_id must be an integer"),
    query("sta_id").isInt().optional().withMessage("sta_id must be an integer"),
    fieldValidator,
  ],
  getAllLoans
);
router.get(
  "/loans/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    fieldValidator,
    getLoanId,
  ],
  getLoanById
);
router.put(
  "/loans/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    body("loa_hour_initial")
      .isInt()
      .optional()
      .withMessage("loa_hour_initial must be an integer"),
    body("loa_hour_final")
      .isInt()
      .optional()
      .withMessage("loa_hour_final must be an integer"),
    body("loa_date")
      .isString()
      .isDate()
      .optional()
      .withMessage("loa_date must be a string with a valida date"),
    body("loa_roo_id")
      .isInt()
      .optional()
      .withMessage("loa_roo_id must be an integer"),
    body("loa_sta_id")
      .isInt()
      .optional()
      .withMessage("loa_sta_id must be an integer"),
    fieldValidator,
    getLoanId,
  ],
  putLoan
);
router.delete(
  "/loans/:id",
  [
    param("id").isInt().notEmpty().withMessage("The id param is required"),
    fieldValidator,
    getLoanId,
  ],
  deleteLoan
);

export default router;
