import db from "../db.js";

export const getLoans = async (
  id = null,
  usr_id = null,
  roo_id = null,
  sta_id = null
) => {
  try {
    const { rows } = await db.query(
      `SELECT out_id as loa_id,
    out_hour_initial as loa_hour_initial,
    out_hour_final as loa_hour_final,
    out_usr_id as loa_usr_id,
    out_usr_name as loa_usr_name,
    out_roo_id as loa_roo_id,
    out_roo_name as loa_roo_name,
    out_sta_id as loa_sta_id,
    out_status as loa_status,
    out_created_at as loa_created_at,
    out_updated_at as loa_updated_at
    FROM get_loans($1, $2, $3, $4)`,
      [id, usr_id, roo_id, sta_id]
    );

    return rows;
  } catch (error) {
    console.error(error);
  }
};
