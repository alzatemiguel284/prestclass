import db from "../db.js";

export const getStatus = async () => {
  try {
    const { rows } = await db.query(`SELECT out_id as sta_id,
    out_name as sta_name,
    out_type as sta_type,
    out_created_at as sta_created_at,
    out_updated_at as sta_updated_at
    FROM get_status()`);

    return rows;
  } catch (error) {
    console.error(error);
  }
};
