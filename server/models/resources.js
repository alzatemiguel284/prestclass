import db from "../db.js";

export const getResources = async (id = null, sta_id = null) => {
  try {
    const { rows } = await db.query(
      `SELECT out_id as res_id,
    out_name as res_name,
    out_description as res_description,
    out_cuantity as res_cuantity,
    out_sta_id as res_sta_id,
    out_status as res_status,
    out_created_at as res_created_at,
    out_updated_at as res_updated_at
    FROM get_resources($1, $2)`,
      [id, sta_id]
    );

    return rows;
  } catch (error) {
    console.error(error);
  }
};
