import db from "../db.js";

export const getSubmenus = async (id = null, rol_id = null, men_id = null) => {
  try {
    const { rows } = await db.query(
      `SELECT out_id as sub_id,
    out_name as sub_name,
    out_description as sub_description,
    out_per_all as sub_per_all,
    out_per_create as sub_per_create,
    out_per_see as sub_per_see,
    out_per_update as sub_per_update,
    out_per_delete as sub_per_delete,
    out_is_visible as sub_is_visible,
    out_men_id as sub_men_id,
    out_men_name as sub_men_name,
    out_men_route as sub_men_route,
    out_rol_id as sub_rol_id,
    out_rol_name as sub_rol_name,
    out_sta_id as sub_sta_id,
    out_status as sub_status,
    out_created_at as sub_created_at,
    out_updated_at as sub_updated_at
    FROM get_submenus($1, $2, $3)`,
      [id, men_id, rol_id]
    );

    return rows;
  } catch (error) {
    console.error(error);
  }
};
