import db from "../db.js";

export const getUsers = async (
  id = null,
  email = null,
  rol_id = null,
  sta_id = null
) => {
  try {
    const { rows } = await db.query(
      `SELECT out_id as usr_id,
    out_name as usr_name,
    out_last_name as usr_last_name,
    out_email as usr_email,
    out_password as usr_password,
    out_rol_id as usr_rol_id,
    out_rol_name as usr_rol_name,
    out_sta_id as usr_sta_id,
    out_status as usr_status,
    out_created_at as usr_created_at,
    out_updated_at as usr_updated_at
    FROM get_users($1, $2, $3, $4)`,
      [id, email, rol_id, sta_id]
    );

    return rows;
  } catch (error) {
    console.error(error);
  }
};
