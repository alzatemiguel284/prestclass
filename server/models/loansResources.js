import db from "../db.js";

export const getLoaRes = async (
  id = null,
  loa_id = null,
  res_id = null,
  sta_id = null
) => {
  try {
    const { rows } = await db.query(
      `SELECT out_id as lre_id,
      out_cuantity as lre_cuantity,
      out_loa_id as lre_loa_id,
      out_res_id as lre_res_id,
      out_res_name as lre_res_name,
      out_sta_id as lre_sta_id,
      out_status as lre_status,
      out_created_at as lre_created_at,
      out_updated_at as lre_updated_at
      FROM get_loans_resources($1, $2, $3, $4)`,
      [id, loa_id, res_id, sta_id]
    );

    return rows;
  } catch (error) {
    console.error(error);
  }
};
