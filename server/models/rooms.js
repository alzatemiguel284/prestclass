import db from "../db.js";

export const getRooms = async (id = null, special = null, sta_id = null) => {
  try {
    const { rows } = await db.query(
      `SELECT out_id as roo_id,
    out_name as roo_name,
    out_special as roo_special,
    out_sta_id as roo_sta_id,
    out_status as roo_status,
    out_created_at as roo_created_at,
    out_updated_at as roo_updated_at
    FROM get_rooms($1, $2, $3)`,
      [id, special, sta_id]
    );

    return rows;
  } catch (error) {
    console.error(error);
  }
};
