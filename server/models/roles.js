import db from "../db.js";

export const getRoles = async () => {
  try {
    const { rows } = await db.query(`SELECT out_id as rol_id,
    out_name as rol_name,
    out_sta_id as rol_sta_id,
    out_status as rol_status,
    out_created_at as rol_created_at,
    out_updated_at as rol_updated_at
    FROM get_roles()`);

    return rows;
  } catch (error) {
    console.error(error);
  }
};
