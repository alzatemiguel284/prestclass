import jwt from "jsonwebtoken";
import { SECRET_JWT } from "../config.js";

export const generateToken = (payload) => {
  return new Promise((resolve, reject) => {
    jwt.sign(payload, SECRET_JWT, { expiresIn: "3d" }, (err, token) => {
      if (err) return reject(err);
      return resolve(token);
    });
  });
};
