import dotenv from "dotenv";
dotenv.config();

process.env.TZ = "UTC";

export const PORT = process.env.PORT || 8080;

export const DBHOST = process.env.DBHOST || "localhost";
export const DBUSER = process.env.DBUSER || "postgres";
export const DBPASS = process.env.DBPASS || "";
export const DBNAME = process.env.DBNAME || "prestclass";
export const DBPORT = process.env.DBPORT || 5432;

export const SECRET_JWT = process.env.SECRET_JWT || "n3W.100k3n_$ecr3t";
